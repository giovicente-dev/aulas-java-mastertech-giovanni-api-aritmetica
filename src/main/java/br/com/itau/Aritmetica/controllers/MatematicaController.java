package br.com.itau.Aritmetica.controllers;

import br.com.itau.Aritmetica.DTOs.EntradaDTO;
import br.com.itau.Aritmetica.DTOs.RespostaDTO;
import br.com.itau.Aritmetica.services.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/matematica")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaService;

    @PutMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entradaDTO) {

        MatematicaController.validarQuantidadeElementos(entradaDTO);
        MatematicaController.validarNumerosNaturais(entradaDTO);

        RespostaDTO resposta = matematicaService.soma(entradaDTO);
        return resposta;
    }

    @PutMapping("/subtracao")
    public RespostaDTO subtracao(@RequestBody EntradaDTO entradaDTO) {

        MatematicaController.validarQuantidadeElementos(entradaDTO);
        MatematicaController.validarNumerosNaturais(entradaDTO);

        RespostaDTO resposta = matematicaService.subtracao(entradaDTO);
        return resposta;
    }

    @PutMapping("/multiplicacao")
    public RespostaDTO multiplicacao(@RequestBody EntradaDTO entradaDTO) {

        MatematicaController.validarQuantidadeElementos(entradaDTO);
        MatematicaController.validarNumerosMaioresQueZero(entradaDTO);

        RespostaDTO resposta = matematicaService.multiplicacao(entradaDTO);
        return resposta;
    }

    @PutMapping("/divisao")
    public RespostaDTO divisao(@RequestBody EntradaDTO entradaDTO) {

        MatematicaController.validarQuantidadeElementos(entradaDTO);
        MatematicaController.validarNumerosMaioresQueZero(entradaDTO);

        for (int i = 0; i < (entradaDTO.getNumeros().size() - 1); i++) {
            if (entradaDTO.getNumeros().get(i) < entradaDTO.getNumeros().get(i + 1)) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Não é permitido dividir um número por um valor maior que ele mesmo.");
            }
        }

        RespostaDTO resposta = matematicaService.divisao(entradaDTO);
        return resposta;
    }

    public static void validarQuantidadeElementos(EntradaDTO entradaDTO) {
        if (entradaDTO.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Quantidade de números enviada é inválida, você enviou " + entradaDTO.getNumeros().size() + " número(s). Envie pelo menos dois números.");
        }
    }

    public static void validarNumerosNaturais(EntradaDTO entradaDTO) {
        for (int numero : entradaDTO.getNumeros()) {
            if (numero < 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Esta API aceita apenas números naturais, você informou o número negativo " + numero + ". Favor informar somente números naturais na entrada.");
            }
        }
    }

    public static void validarNumerosMaioresQueZero(EntradaDTO entradaDTO) {
        for (int numero : entradaDTO.getNumeros()) {
            if (numero <= 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Esta API aceita apenas números naturais, e para esta operação é proibido informar zero, você informou o número " + numero + ". Favor informar somente números válidos na entrada.");
            }
        }
    }
}
