package br.com.itau.Aritmetica.DTOs;

public class RespostaDTO {
    private int resultado;

    public RespostaDTO(){}

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }
}
