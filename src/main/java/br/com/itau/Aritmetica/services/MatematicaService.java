package br.com.itau.Aritmetica.services;

import br.com.itau.Aritmetica.DTOs.EntradaDTO;
import br.com.itau.Aritmetica.DTOs.RespostaDTO;
import org.springframework.stereotype.Service;

@Service
public class MatematicaService {

    public RespostaDTO soma(EntradaDTO entradaDTO){
        int resultado = 0;

        for (int numero : entradaDTO.getNumeros()){
            resultado += numero;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(resultado);
        return resposta;
    }

    public RespostaDTO subtracao(EntradaDTO entradaDTO){
        int numero = 0;
        int resultado = 0;

        for(int i = 0; i < entradaDTO.getNumeros().size(); i++){
            if(i == 0){
                resultado = entradaDTO.getNumeros().get(i);
            } else {
                numero = entradaDTO.getNumeros().get(i);
                resultado -= numero;
            }
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(resultado);
        return resposta;
    }

    public RespostaDTO multiplicacao(EntradaDTO entradaDTO){
        int resultado = 1;

        for (int numero : entradaDTO.getNumeros()){
            resultado *= numero;
        }
        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(resultado);
        return resposta;
    }

    public RespostaDTO divisao(EntradaDTO entradaDTO){
        int resultado = 0;
        int numero = 0;

        for(int i = 0; i < entradaDTO.getNumeros().size(); i++){
            if(i == 0){
                resultado = entradaDTO.getNumeros().get(i);
            } else {
                numero = entradaDTO.getNumeros().get(i);
                resultado /= numero;
            }
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(resultado);
        return resposta;
    }
}
